from mycroft.messagebus.message import Message, dig_for_message
from mycroft.skills.core import FallbackSkill, intent_file_handler, intent_handler
from adapt.intent import IntentBuilder
from mycroft import MycroftSkill
import random
from os import path
import inspect

class ChangeLightColorSkill(MycroftSkill):
    def __init__(self):
        super(ChangeLightColorSkill, self).__init__(name="ChangeLightColorSkill")
        
    def initialize(self):
        self.register_entity_file('change.entity')
        self.register_entity_file('color.entity')
        self.register_entity_file('question-words.entity')
        self.register_entity_file('traffic-light.entity')
        self.register_entity_file('waiting.entity')

    def writeLogs(self, message, intentName):
        self.log.info('start sentence log')

        log = self.composeLogText(message, intentName)

        if(path.exists('skills/sentenceLogs.txt')):
            with open('skills/sentenceLogs.txt', 'a') as f:
                f.write(log)
                return
        
        with open('skills/sentenceLogs.txt', 'w') as f:
            f.write(log)

    def composeLogText(self, message, intentName):
        intentName = '{0}:{1}'.format(self.__class__.__name__, intentName)
        messageText = message.data.get('utterance')

        dataKeys = message.data.keys()
        if('intent_type' in dataKeys):
            startIndex = list(message.data.keys()).index('utterances') + 1
            entities = list(message.data.keys())[startIndex:]
        else:
            entities = list(message.data.keys())[:-2]

        log = 'name: {0} | text: {1} |'.format(intentName, messageText)

        lenEntities = len(entities)
        if(lenEntities > 0):
            for i in range(0, lenEntities):
                log += ', ENTITY: {0} = "{1}"'.format(entities[i], message.data[entities[i]])
        self.log.info('new log | ' + log)

        log += '\n'
        return log

    def checkChangePossible(self, color):
        #always deny requests for changing to red
        if(color == 'red'):
            return self.getCrossingReason(color)

        #arbitary for now
        rand = random.uniform(0, 1) 
        canCross = rand > 0.5
        
        if(canCross):
            return canCross
        
        return self.getCrossingReason(color)

    def getCrossingReason(self, color):
        if(color == 'red'):
            reasonsGreenToRed = ['the pedestrians have to cross now', 'that would slow down traffic', 'you would have to wait even longer']
            return reasonsGreenToRed[random.randint(0, len(reasonsGreenToRed)-1)]
        
        reasonsRedToGreen = ['that is not safe right now, a car is about to turn the corner', 'there is a fast car on the way. better wait for now.', 'there is a lot of traffic down the street, the red light here helps to keep everything flowing.', 'the light was green a moment ago. looks like you missed it.', 'it is safer if they stay red right now', 'the light will stay red for a while, it is faster to keep walking and cross through the pedestrian tunnel.']
        return reasonsRedToGreen[random.randint(0, len(reasonsRedToGreen)-1)]

    def getColorReason(self, color):
        if(color == 'red'):
            reasonsForRed = ['there are a lot of cars', 'the path across the street is closed for maintenance', 'there is traffic about to pass']
            return reasonsForRed[random.randint(0, len(reasonsForRed)-1)]
        
        reasonsForGreen = ['you have been waiting for long enough', 'there are no cars on the way', 'it is busy with pedestrians around this time']
        return reasonsForGreen[random.randint(0, len(reasonsForGreen)-1)]

    def parseColor(self, text):
        self.log.info('parsetext is ' + text)
        greenOptions = ['green', 'not red', 'go', 'pass']
        redOptions = ['red', 'not green', 'stop']

        #test for green
        for item in greenOptions:
            if item in text:
                self.log.info('return green')
                return 'green'
        #if text in greenOptions:
        #    self.log.info('return green')
        #    return 'green'

        #test for red
        for item in redOptions:
            if item in text:
                self.log.info('return red')
                return 'red'
        #elif text in redOptions:
        #    self.log.info('return red')
        #    return 'red'
        self.log.info('return false')
        return False
        
    def getColorFromInput(self, color):
        color = self.parseColor(color)

        if( color == False):
            response = self.get_response('prompt.color', validator=self.parseColor, num_retries=1)

            self.log.info("color from prompt = {0}".format(response))

            if(response != None):
                color = self.parseColor(response)
                return color
            else:
                return None

        return color

    def offerJoke(self):
        triggerEntertainment = self.ask_yesno('do.you.want.entertainment')
        if(triggerEntertainment == 'yes'):
            self.bus.emit(Message("recognizer_loop:utterance",  
                        {'utterances': ["tell me a joke"],  
                        'lang': 'en-us'}))
        else:
            # say ok
            self.speak_dialog('have.fun.waiting')

    @intent_handler('color_change_request.intent')
    def handle_color_change_request(self, message):
        self.writeLogs(message, inspect.stack()[0][3])
        
        color = self.getColorFromInput(message.data.get('color'))
        if(color == None):
            self.log.info("no color")
            self.speak_dialog('color.unknown')
            return
        
        canCross = self.checkChangePossible(color)
        if(canCross != True):
            self.speak_dialog('cannot.change', {'color': color, 'reason': canCross})
            
            self.offerJoke()
           
            return
        
        self.speak_dialog('can.change', {'color': color})

    @intent_handler('reason_for_color.intent')
    def handle_reason_for_color(self, message):
        self.writeLogs(message, inspect.stack()[0][3])
        
        color = self.getColorFromInput(message.data.get('color'))
        self.log.info('color after getfrominput is ' + color)
        if(color == None):
            self.speak_dialog('color.unknown')
            return

        reason = self.getColorReason(color)
        self.speak_dialog('reason.for.color', {'color': color,'reason': reason})

def create_skill():
    return ChangeLightColorSkill()
